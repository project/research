<?php
/**
* Implementation of hook_node_info().
*/
function research_node_info() {
  return array(
    'research_participant' => array(
      'name' => t('Research Participant'),
      'module' => 'research',
      'description' => "Node that holds all information about one research participant.",
    )
  );
}


/**
* Implementation of hook_perm().
*/

function research_perm() {
  return array('create research participant', 'edit own research participants');
} 


/**
* Implementation of hook_access().
*/

function research_access($op, $node, $account) {

  if ($op == 'create') {
    // Only users with permission to do so may create this node type.
    return user_access('create research_participant', $account);
  }

  // Users who create a node may edit or delete it later, assuming they have the
  // necessary permissions.
  if ($op == 'update' || $op == 'delete') {
    if (user_access('edit own research_participant', $account) && ($account->uid == $node->uid)) {
      return TRUE;
    }
  }
}


/**
* Implementation of hook_form().
*/

function research_form(&$node) {
  $type = node_get_types('type', $node);

  // We need to define form elements for the node's title and body.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#weight' => -5
  );
  // We want the body and filter elements to be adjacent. We could try doing
  // this by setting their weights, but another module might add elements to the
  // form with the same weights and end up between ours. By putting them into a
  // sub-array together, we're able force them to be rendered together.
  $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => check_plain($type->body_label),
    '#default_value' => $node->body,
    '#required' => FALSE
  );
  $form['body_filter']['filter'] = filter_form($node->format);

  // NOTE in node_example there is some addition code here not needed for this simple node-type

  return $form;
}